---

## Notifications ##

---

# README #

This repository will be used for the exercise of the the Data Mining II course conducted at the Otto von Guericke University during the Summer Semester 2017. 

1. The exercise classes will comprise of questions and programming assignments. The purpose of the programming assignments is to get a better understanding of the concepts introduced in the lecture and to get familiar with tools used for data mining.

2. You are free to use any programming language and libraries you are comfortable with. There are no pre-requisites required to be completed in order to be able to appear for the exams. This means that the exercises and programming assignments are not mandatory.

3. The programming assignments must be done in groups of 4-5 students. It is highly encouraged that the groups come prepared with solutions for the programming assignments and present them in class.

4. For the programming assignments we will primarily use the [Yelp Dataset Challenge](https://www.yelp.com/dataset_challenge). The Yelp Dataset consists of reviews of different businesses across different cities. For the duration of the course, each group will be assigned a different city on which they need solve the programming assignments.

5. Please send the members of your group to Elson Serrao at (elson.serrao@ovgu.de) so that a city can be assigned to your group latest by 15.05.2017. In the mail please mention who will be the team leader and the Bitbucket account Id's all members of the group.

6. Instructions on how to get started, code snippets, scripts, etc. will regularly be uploaded in the master branch of the [Data Mining II repository](https://bitbucket.org/eserrao/data-mining-ii).

7. A separate repository will be created for each group which can be used by the members of the group to co-ordinate their work.

### How do I get set up? ###

* **Downloading the Yelp Dataset**
The Yelp Dataset can be downloaded from [here](https://www.yelp.de/dataset_challenge/dataset). Every member of the group needs to agree to the Terms of Use before using the dataset. The download is about 1.9GB. Once the download is complete, its contents exrtact to about 5GB. You will find the JSON format files in the extraction.

* **Configuration**
Copy the JSON files to `/data/json/`.

* **Convert to csv**
The dataset files are in JSON format. The format of the files are mentioned on the [Yelp Dataset Challenge](https://www.yelp.com/dataset_challenge) website. If you need to convert to JSON format files to CSV, you can use the corresponding scripts available in `/scripts/`

* **Extract City Data**
To extract only the data required for the city assigned, you can use the corresponding script available in `/scripts/`. This script uses the CSV files generated in the above step.

* **Play around with the Dataset**
You are all setup and ready to start with the exercises. You can use the `Getting Started.ipynb` to have a glimpse of the data and just play around with the data.

* **Getting Started with Python and Jupyter Notebooks**
If you are using the scripts and need to install python I would suggest installing [Anaconda](https://www.continuum.io/downloads) as it takes care of most of the installation hassels. The installation steps are provided [here](http://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/install.html)

### Contribution guidelines ###

* Members of a group will have Write access only on the repository assigned to them.
* Please regularly sync your repository to receive updates
* To avoid any merge conflicts during synchronization, please solve your solutions in a folder with its name as the city name. For example, if the city 'New York City' has been assigned to your team, create a folder `/New York City` for your solutions.
* If you wish to share your results and code with the others, please create a pull request and I will merge your repository into the main repository.

### Programming Guides in Python ###

* [10 Minutes to Pandas](http://pandas.pydata.org/pandas-docs/stable/10min.html)
* [NumPy Quickstart Tutorial](https://docs.scipy.org/doc/numpy-dev/user/quickstart.html)
* [Python NLTK Book](http://www.nltk.org/book/) 

### Frameworks that Support Data Streams ###
* [Apache Spark](http://spark.apache.org/)
* [Apache Flink](https://flink.apache.org/)
* [Apache Storm](http://storm.apache.org/)
* [Apache Mahout](http://mahout.apache.org/)
* [MOA (Massive Online Analysis)](http://moa.cms.waikato.ac.nz/)

### Some Additional Reading ###
* [Window Operations in Spark Structured Streaming](http://spark.apache.org/docs/latest/structured-streaming-programming-guide.html#operations-on-streaming-dataframesdatasets)

### Contact ###

Incase of any problems or issues please contact Elson Serrao at elson.serrao@ovgu.de