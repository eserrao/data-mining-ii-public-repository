This folder contains the Yelp Dataset Challenge files in csv format. The `json_to_csv_converter` scripts generates the respective csv files in this folder. After running the script the files generated here are:
- yelp_academic_dataset_business.csv
- yelp_academic_dataset_review.csv
- yelp_academic_dataset_user.csv
- yelp_academic_dataset_tip.csv
- yelp_academic_dataset_checkin.csv

Note: As the size of the files are approximately 4.1GB please do not commit these files in the repository. As a precaution they have been added to .gitignore