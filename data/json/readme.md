This folder contains the Yelp Dataset Challenge files in json format. Once you download the Dataset copy the below files to this folder
- yelp_academic_dataset_business.json
- yelp_academic_dataset_review.json
- yelp_academic_dataset_user.json
- yelp_academic_dataset_tip.json
- yelp_academic_dataset_checkin.json

Note: As the size of the files are approximately 4.1GB please do not commit these files in the repository. As a precaution they have been added to .gitignore